package projecttest

type Test struct {
	I int `json:"i"`
	S string `json:"s"`
	B bool `json:"b"`
}
